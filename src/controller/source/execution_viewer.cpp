/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
/*
  ExecutionViewer
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/execution_viewer.h"

ExecutionViewer::ExecutionViewer(int argc, char** argv, QWidget* parent) : QWidget(parent), ui(new Ui::ExecutionViewer)
{
  QWidget::setLocale(QLocale());

  ui->setupUi(this);

  // window always on top
  Qt::WindowFlags flags = windowFlags();
  setWindowFlags(flags | Qt::WindowStaysOnTopHint);
  setWindowIcon(QIcon(":/img/img/execution_viewer.png"));
  setWindowTitle("Execution Viewer");
  qRegisterMetaType<QVector<int>>("QVector<int>");
  my_layout = ui->gridLayout;
  this->point = 0;

  belief_label = new QLabel("Belief Viewer", this);
  behavior_label = new QLabel("Behavior Viewer", this);
  behavior_content = new QTableWidget(this);
  belief_content = new QTableWidget(this);

  QSizePolicy behavior_policy = QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  QSizePolicy belief_policy = QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  behavior_policy.setVerticalStretch(1);
  belief_policy.setVerticalStretch(5);
  behavior_content->setSizePolicy(behavior_policy);
  belief_content->setSizePolicy(belief_policy);

  my_layout->addWidget(behavior_label, 0, 0);
  my_layout->addWidget(behavior_content, 1, 0);
  my_layout->addWidget(belief_label, 2, 0);
  my_layout->addWidget(belief_content, 3, 0);

  behavior_label->show();
  behavior_content->show();
  belief_label->show();
  belief_content->show();

  this->is_behavior_context_menu_created = false;
  this->is_belief_context_menu_created = false;

  n.param<std::string>("cancel_behavior", cancel_behavior, "request_behavior_deactivation");
  n.param<std::string>("remove_belief", remove_belief, "remove_belief");
  n.param<std::string>("all_beliefs", all_beliefs, "all_beliefs");
  n.param<std::string>("list_of_active_behaviors", list_of_active_behaviors, "list_of_active_behaviors");
  n.param<std::string>("robot_namespace", drone_id_namespace, "drone1");

  cancel_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehaviorDeactivation>(cancel_behavior);
  remove_belief_srv = n.serviceClient<droneMsgsROS::RemoveBelief>(remove_belief);
  list_of_behaviors_sub = n.subscribe("/" + drone_id_namespace + "/" + list_of_active_behaviors, 1000,
                                      &ExecutionViewer::listOfBehaviorsCallback, this);
  std::cout << "/" + drone_id_namespace + "/" + list_of_active_behaviors << std::endl;
  list_of_beliefs_sub =
      n.subscribe("/" + drone_id_namespace + "/" + all_beliefs, 1000, &ExecutionViewer::listOfBeliefsCallback, this);

  setUpBehaviorListTable();

  // reads layout file
  namespace pt = boost::property_tree;

  std::string layout_dir = std::getenv("AEROSTACK_STACK") + std::string("/stack/ground_control_system/"
                                                                        "graphical_user_interface/layouts/layout.json");

  pt::read_json(layout_dir, root);

  QScreen* screen = QGuiApplication::primaryScreen();
  QRect screenGeometry = screen->geometry();

  int y0 = screenGeometry.height() / 2;
  int x0 = screenGeometry.width() / 2;
  int height = root.get<int>("EXECUTION_VIEWER.height");
  int width = root.get<int>("EXECUTION_VIEWER.width");

  this->resize(width, height);
  this->move(x0 + root.get<int>("EXECUTION_VIEWER.position.x"), y0 + root.get<int>("EXECUTION_VIEWER.position.y"));

  // Settings connections
  setUp();
}

ExecutionViewer::~ExecutionViewer()
{
  delete ui;
}

void ExecutionViewer::setUp()
{
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  n.param<std::string>("window_event_topic", window_event_topic, "window_event");


  // Subscribers
  window_event_sub =
      n.subscribe("/" + drone_id_namespace + "/" + window_event_topic, 10, &ExecutionViewer::windowOpenCallback, this);

  // Publishers
  window_event_pub =
      n.advertise<aerostack_msgs::WindowEvent>("/" + drone_id_namespace + "/" + window_event_topic, 1, true);
}

void ExecutionViewer::listOfBehaviorsCallback(const droneMsgsROS::ListOfBehaviors& msg)
{
  this->behavior_content->setEditTriggers(QAbstractItemView::NoEditTriggers);
  this->clearBehaviorTable();
  this->all_behaviors_active.clear();
  this->behavior_content->setRowCount(msg.behaviors.size());

  for (int i = 0; i < msg.behaviors.size(); i++)
  {
    std::cout << msg.behaviors[i] << std::endl;
    std::cout << deSerialization(msg.behavior_commands[i].arguments) << std::endl;
    // For each behavior active, add a child to the tree
    this->all_behaviors_active.push_back(msg.behaviors[i]);
    QTableWidgetItem* new_behavior = new QTableWidgetItem(QString::fromStdString(msg.behaviors[i]));
    QTableWidgetItem* new_arguments =
        new QTableWidgetItem(QString::fromStdString(deSerialization(msg.behavior_commands[i].arguments)));

    this->behavior_content->setItem(i, 0, new_behavior);
    this->behavior_content->setItem(i, 1, new_arguments);
  }
}

void ExecutionViewer::listOfBeliefsCallback(const aerostack_msgs::ListOfBeliefs& msg)
{
  this->belief_content->setEditTriggers(QAbstractItemView::NoEditTriggers);

  std::this_thread::sleep_for(std::chrono::milliseconds(100));  // Needed to adapt all_beliefs topic frequency to GUI
                                                                // frequency
  this->clearBeliefTable();
  this->all_beliefs_content.clear();
  std::stringstream stream(msg.beliefs);
  std::string one_belief;

  int i = 0;
  while (std::getline(stream, one_belief))
  {
    // std::cout<<one_belief<<std::endl;
    this->belief_content->setRowCount(i + 1);
    all_beliefs_content.push_back(one_belief);
    QTableWidgetItem* new_belief = new QTableWidgetItem(QString::fromStdString(one_belief));
    this->belief_content->setItem(i, 0, new_belief);
    i++;
  }
}

void ExecutionViewer::setUpBehaviorListTable()
{
  QStringList* headers = new QStringList();
  headers->append("Behavior Name");
  headers->append("Arguments");
  this->behavior_content->setColumnCount(2);
  this->behavior_content->verticalHeader()->setVisible(false);
  this->behavior_content->setHorizontalHeaderLabels(*headers);
  this->behavior_content->horizontalHeader()->setVisible(true);
  this->behavior_content->setSortingEnabled(false);
  this->behavior_content->adjustSize();
  this->behavior_content->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(behavior_content, SIGNAL(customContextMenuRequested(const QPoint&)), this,
          SLOT(behaviorCustomContextMenu(const QPoint&)));
  this->behavior_content->viewport()->setFocusPolicy(Qt::NoFocus);

  this->belief_content->setColumnCount(1);
  this->belief_content->verticalHeader()->setVisible(false);
  this->belief_content->horizontalHeader()->setVisible(false);
  this->belief_content->setSortingEnabled(false);
  this->belief_content->adjustSize();
  this->belief_content->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(belief_content, SIGNAL(customContextMenuRequested(const QPoint&)), this,
          SLOT(beliefCustomContextMenu(const QPoint&)));
  this->belief_content->viewport()->setFocusPolicy(Qt::NoFocus);
}

void ExecutionViewer::behaviorCustomContextMenu(const QPoint& p)
{
  if (this->behavior_content->itemAt(p) != 0)
  {
    if (is_behavior_context_menu_created)
    {
      behavior_context_menu->close();
      delete behavior_context_menu;
      is_behavior_context_menu_created = false;
    }
    behavior_context_menu = new QMenu("Menu", this);
    is_behavior_context_menu_created = true;
    this->point = new QPoint(p);

    QAction action_add_behavior("Add a behavior", this->behavior_content);
    QAction action_stop_behavior("Stop the execution of this behavior", this->behavior_content);

    behavior_context_menu->addAction(&action_add_behavior);
    behavior_context_menu->addAction(&action_stop_behavior);

    connect(&action_add_behavior, SIGNAL(triggered()), this, SLOT(addBehavior()));
    connect(&action_stop_behavior, SIGNAL(triggered()), this, SLOT(stopBehavior()));

    behavior_context_menu->exec(QCursor::pos());
  }
  else
  {
    if (is_behavior_context_menu_created)
    {
      behavior_context_menu->close();
      delete behavior_context_menu;
      is_behavior_context_menu_created = false;
    }
    behavior_context_menu = new QMenu("Menu", this);
    is_behavior_context_menu_created = true;
    this->point = 0;

    QAction action_add_behavior("Add a behavior", this->behavior_content);

    behavior_context_menu->addAction(&action_add_behavior);

    connect(&action_add_behavior, SIGNAL(triggered()), this, SLOT(addBehavior()));

    behavior_context_menu->exec(QCursor::pos());
  }
}

void ExecutionViewer::beliefCustomContextMenu(const QPoint& p)
{
  if (this->belief_content->itemAt(p) != 0)
  {
    if (is_belief_context_menu_created)
    {
      belief_context_menu->close();
      delete belief_context_menu;
      is_belief_context_menu_created = false;
    }
    belief_context_menu = new QMenu(tr("Menu"), this->belief_content);
    is_belief_context_menu_created = true;
    this->point = new QPoint(p);

    QAction action_add_belief("Add a belief", this->belief_content);
    QAction action_remove_belief("Remove this belief", this->belief_content);

    belief_context_menu->addAction(&action_add_belief);
    belief_context_menu->addAction(&action_remove_belief);

    connect(&action_add_belief, SIGNAL(triggered()), this, SLOT(addBelief()));
    connect(&action_remove_belief, SIGNAL(triggered()), this, SLOT(removeBelief()));

    belief_context_menu->exec(QCursor::pos());
  }
  else
  {
    if (is_belief_context_menu_created)
    {
      belief_context_menu->close();
      delete belief_context_menu;
      is_belief_context_menu_created = false;
    }
    belief_context_menu = new QMenu(tr("Menu"), this->belief_content);
    is_belief_context_menu_created = true;
    this->point = 0;

    QAction action_add_belief("Add a belief", this->belief_content);

    belief_context_menu->addAction(&action_add_belief);

    connect(&action_add_belief, SIGNAL(triggered()), this, SLOT(addBelief()));

    belief_context_menu->exec(QCursor::pos());
  }
}

void ExecutionViewer::addBehavior()
{
  ExecutionViewerDialog* dialog = new ExecutionViewerDialog(this, 0, 0);
  dialog->show();
}

void ExecutionViewer::addBelief()
{
  ExecutionViewerDialog* dialog = new ExecutionViewerDialog(this, 1, 0);
  dialog->show();
}

void ExecutionViewer::stopBehavior()
{
  QTableWidgetItem* item = this->behavior_content->itemAt(*point);
  if (item->column() != 0)
  {
    item = this->behavior_content->item(item->row(), item->column() - 1);
  }
  ExecutionViewerDialog* dialog = new ExecutionViewerDialog(this, 2, item);
}

void ExecutionViewer::removeBelief()
{
  ExecutionViewerDialog* dialog = new ExecutionViewerDialog(this, 3, belief_content->itemAt(*point));
}

void ExecutionViewer::clearBehaviorTable()
{
  this->behavior_content->setRowCount(0);
}

void ExecutionViewer::clearBeliefTable()
{
  this->belief_content->setRowCount(0);
}

void ExecutionViewer::resizeEvent(QResizeEvent* event)
{
  this->behavior_content->setColumnWidth(0, this->behavior_content->width() / 2);
  this->behavior_content->setColumnWidth(1, (this->behavior_content->width() / 2) - 2);
  this->belief_content->setColumnWidth(0, this->belief_content->width());
  // this->belief_content->setColumnWidth(1, this->belief_content->width()/2);
  QWidget::resizeEvent(event);
}

const std::string ExecutionViewer::deSerialization(const std::string input)
{
  std::string de_serialization_result;
  for (int j = 0; j < input.size(); j++)
  {
    if (input[j] != '\'' && input[j] != '}' && input[j] != '{')
    {
      de_serialization_result = de_serialization_result + input[j];
    }
  }
  return de_serialization_result;
}
void ExecutionViewer::closeEvent(QCloseEvent* event)
{
  window_event_msg.window = aerostack_msgs::WindowEvent::EXECUTION_VIEWER;
  window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
  window_event_pub.publish(window_event_msg);
}

void ExecutionViewer::killMe()
{
#ifdef Q_OS_WIN
  enum
  {
    ExitCode = 0
  };
  ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
  qint64 pid = QCoreApplication::applicationPid();
  QProcess::startDetached("kill -9 " + QString::number(pid));
#endif  // Q_OS_WIN
}

void ExecutionViewer::windowOpenCallback(const aerostack_msgs::WindowEvent& msg)
{
 
  if ( msg.window == aerostack_msgs::WindowEvent::ALPHANUMERIC_INTERFACE_CONTROL )
  {
     window_event_msg.window = aerostack_msgs::WindowEvent::EXECUTION_VIEWER;
     window_event_msg.event = aerostack_msgs::WindowEvent::CLOSE;
     window_event_pub.publish(window_event_msg);
     killMe();
  }

  if (msg.window == aerostack_msgs::WindowEvent::INTEGRATED_VIEWER && msg.event == aerostack_msgs::WindowEvent::MINIMIZE)
    showMinimized();
}
